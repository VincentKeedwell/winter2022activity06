import java.nio.file.*;
import java.util.*;

public class ShapeReader {
    public static void main(String[] args) throws Exception {
       /*RightTriangle[] triangles = loadTriangles("triangles.csv");
        printTriangles(triangles);
		RightTriangle testTriangle = new RightTriangle(3.0, 4.0);
		System.out.println(testTriangle);
		System.out.println(testTriangle.getArea());*/
		
		RightTriangle testT = new RightTriangle(3.0, 4.0);
		RightTriangle testT2 = doubleSizeNewTriangle(testT);
		System.out.println(testT2);
		
		/*double newBase = testT.getBase() * 2;
		double newHeight = testT.getHeight() * 2;
		RightTriangle newTriangle = new RightTriangle(newHeight, newBase);
		System.out.println(newTriangle.toString());*/
		
		RightTriangle testT3 = doubleSizeOldTriangle(testT);
		System.out.println(testT3.toString());
    }

    public static RightTriangle[] loadTriangles(String path) throws Exception {
        List<String> linesAsList = Files.readAllLines(Paths.get(path));
        String[] lines = linesAsList.toArray(new String[0]);

        RightTriangle[] triangles = new RightTriangle[lines.length];
        for (int i = 0; i < lines.length; i++) {
            String[] pieces = lines[i].split(",");
            triangles[i] = new RightTriangle(Double.parseDouble(pieces[0]), Double.parseDouble(pieces[1]));
        }

        return triangles;
    }

    public static void printTriangles(RightTriangle[] triangles) {
        for (RightTriangle triangle : triangles) {
            System.out.println(triangle);
        }
    }
	
	public static RightTriangle doubleSizeNewTriangle(RightTriangle triangle) {
		double newBase = triangle.getBase() * 2;
		double newHeight = triangle.getHeight() * 2;
		RightTriangle newTriangle = new RightTriangle(newHeight, newBase);
		return newTriangle;
	}
	
	triangle = new RightTriangle(triangle.getHeight(), triangle.getBase());
	public static void doubleSizeOldTriangle(RightTriangle triangle) {
		triangle.setBase(triangle.getBase() * 2);
		triangle.setHeight(triangle.getHeight() * 2);
	}
}