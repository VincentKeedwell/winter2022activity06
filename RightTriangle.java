public class RightTriangle {
    private double height;
    private double base;

    public RightTriangle(double h, double b) {
        this.height = h;
        this.base = b;
    }

    public double getArea() {
        return (1.0 / 2.0) * this.height * this.base;
    }

    public double getPerimeter() {
        return this.height + this.base + this.getHypotenuse();
    }

    public double getHypotenuse() {
        return Math.sqrt(this.height * this.height + this.base * this.base);
    }
	
	public double getBase() {
		return this.base;
	}
	
	public double getHeight() {
		return this.height;
	}
	
	public void setBase(double base) {
		this.base = base;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
    public String toString() {
        return "Height: " + this.height + ", Width: " + this.base + " , Area: " + this.getArea() + " Perimeter: " + this.getPerimeter();
    }
	
	
}
